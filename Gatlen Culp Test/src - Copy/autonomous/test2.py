from magicbot import AutonomousStateMachine, timed_state, default_state, state
import wpilib

# Simply used to test if the autonomous works using the AutonomousStateMachine
# Ditch the 3 initial methods
# Requires a default state
# https://robotpy-wpilib-utilities.readthedocs.io/en/latest/magicbot.html#module-magicbot.state_machine
class Test2(AutonomousStateMachine):

    MODE_NAME = "Test2"
    DEFAULT = False

    @timed_state(duration=5, first=True, next_state="reeeeeScream")
    def autonomousScream(self):
        # self.next_state_now("ReeeeeScream")
        # self.next_state("reeeeeScream")
        print("AUUUUUUUTONOOOMOUUUSS")

    @timed_state(duration=5, next_state="turnMeOffScream")
    def reeeeeScream(self):
        print("REEEEEEEEEEE")

    @timed_state(duration=5)
    def turnMeOffScream(self):
        print("TURN ME OOOOOOOOF")

    @default_state
    def defaultScream(self):
        print("AAAAAAAAAAAAAAH")
