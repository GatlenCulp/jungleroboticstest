'''
Created by Gatlen Culp 23 December 2018

This is the Master File that runs all the other files.
'''

import wpilib, magicbot
from robotmap import RobotMap as Map
from components.drive import Drive

def deadZone(value, deadzone):
    if abs(value) < deadzone: return 0
    else: return value

# MagicRobot api: https://robotpy.readthedocs.io/en/latest/frameworks/magicbot.html
class Robot(magicbot.MagicRobot):
    drive: Drive

    def createObjects(self):

        # ==== SETUP DRIVE ====

        # Injects this talon information back to the component itself by means
        # of variable attributes. Should be proceeded by drive_, done by MagicRobot
        self.drive_injection_check = "Check completed, drive injection success"

        self.drive_fl_motor = wpilib.Talon(Map.pwm.drive.FL_CANTALON_MC)
        self.drive_bl_motor = wpilib.Talon(Map.pwm.drive.BL_CANTALON_MC)
        self.drive_left = wpilib.SpeedControllerGroup(self.drive_fl_motor, self.drive_bl_motor)
        # Must set inverted or else you have to put in a negative power for whatever reason
        self.drive_left.setInverted(True)

        self.drive_fr_motor = wpilib.Talon(Map.pwm.drive.FR_CANTALON_MC)
        self.drive_br_motor = wpilib.Talon(Map.pwm.drive.BR_CANTALON_MC)
        self.drive_right = wpilib.SpeedControllerGroup(self.drive_fr_motor, self.drive_br_motor)
        self.drive_right.setInverted(True)

        self.drive_controller = wpilib.RobotDrive(self.drive_left, self.drive_right)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.Joystick(Map.joystick.XBOX_CONTROLLER_PORT)

    def disabledPeriodic(self):
        # Ensure motors are turned off when disabled
        self.drive_controller.tankDrive(0, 0)

    def teleopInit(self):
        pass

    def teleopPeriodic(self):
        # Any values not past the deadzone won't register because the stick isn't
        # perfectly straight when not touched
        analog_stick_deadzone = 0.2
        # They legtimately changed the controller mapping to be even harder
        a_pressed = self.xbox.getRawButton(Map.joystick.A)
        l_stick_x = deadZone(self.xbox.getRawAxis(Map.joystick.L_STICK_X), analog_stick_deadzone)
        l_stick_y = deadZone(self.xbox.getRawAxis(Map.joystick.L_STICK_Y), analog_stick_deadzone)
        r_stick_y = deadZone(self.xbox.getRawAxis(Map.joystick.R_STICK_Y), analog_stick_deadzone)
        # Invert so RT is positive, just easier for me personally, multiply by 2
        # so it goes full speed when fully pressed, gets capped when you enter it for speed
        speed = triggers = self.xbox.getRawAxis(Map.joystick.TRIGGERS) * -2

        # Below is used to test if the buttons and axes work
        # if a_pressed:
        #     o = ""
        #     for button in range(1, 11):
        #         try:
        #             o += str(button) + " " + str(self.xbox.getRawButton(button)) + " "
        #         except:
        #             o += "Error on " + str(button) + " "
        #     util.debug(o, title="Buttons")
        #
        #     o2 = ""
        #     for axis in range(6):
        #         try:
        #             o2 += str(axis) + " " + str(self.xbox.getRawAxis(axis)) + "\n"
        #         except:
        #             o2 += "Error on " + str(axis) + " "
        #     util.debug(o2, title="Axes")

        '''Regular analog tank drive'''
        # self.drive_controller.tankDrive(l_stick_y * -1, r_stick_y * -1)

        '''Tank drive with smoother controls. LT to go backward, RT to go forward,
        and use the left analog stick to control turning.'''
        normalizer = 0.25

        dist_from_left = (1 + l_stick_x)/2 # 0 when all left, 0.5 when dead center, 1 when all right
        l_speed_ratio = dist_from_left + normalizer
        r_speed_ratio = 1 - dist_from_left + normalizer
        self.drive_controller.tankDrive(speed * l_speed_ratio, speed * r_speed_ratio)

if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
