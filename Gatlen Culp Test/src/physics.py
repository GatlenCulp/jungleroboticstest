# from pyconsole_util import util
from robotmap import RobotMap as Map
from sim.config import robot_info
from pyfrc.physics.tankmodel import TankModel
from pyfrc.physics.units import units

class PhysicsEngine(object):
    # Required class for physics
    first_run = True

    def __init__(self, physics_controller):
        # constructor must accept a physics_controller
        self.physics_controller = physics_controller

        # Sets up the physics for a Tank Drive robot
        self.drive = TankModel.theory(
                motor_config=robot_info["motor_type"],
                robot_mass=robot_info["weight"] * units.lbs,
                gearing=robot_info["gear_ratio"],
                nmotors=robot_info["motors_per_side"],
                robot_width=(robot_info["width"] + robot_info["bumper_width"] * 2) * units.inch,
                robot_length=(robot_info["height"] + robot_info["bumper_width"] * 2) * units.inch,
                # We actually use treads lol but this is needed
                wheel_diameter=6 * units.inch
            )


    def initialize(self, hal_data):
        pass

    def update_sim(self, hal_data, now, tm_diff):
        # Called each time phyiscs updates
        # hal_data is a dictionary of robot data
        # now is the current time
        # tm_diff is the amount of time since this was last updated
        if self.first_run:
            self.first_run = False

        # hal_data["pwm"] contains all the information from the robot setup in robot.py
        # Saves front left and right motor info, only takes the value and applies to whole channel
        l_motor = hal_data["pwm"][Map.pwm.drive.FL_CANTALON_MC]["value"]
        r_motor = hal_data["pwm"][Map.pwm.drive.FR_CANTALON_MC]["value"]

        # Saves the displacement the robot has made and then applies that to the sim
        x, y, angle = self.drive.get_distance(l_motor, r_motor, tm_diff)
        self.physics_controller.distance_drive(x, y, angle)
