'''
This script serves to create a list of ports and channels that can be easily
changed if someone were to say- change the port a motor was plugged into on the
RoboRIO
'''

# List of ports for motor controllers used for driving on the RoboRIO (feeds into PWM)
class Drive:
    # It's supposed to be a CANTalon but I don't know what that is- TalonSR, TalonSRX, or a canified Talon idk
    FR_CANTALON_MC = 0
    BL_CANTALON_MC = 1
    BR_CANTALON_MC = 2
    FL_CANTALON_MC = 4

# List of PWM ports on the RoboRIO (feeds into RobotMap)
class PWM:
    drive = Drive()
    WINCH = 4
    GEAR = 5

# List of buttons/triggers/analog sticks for the xbox controller (feeds into RobotMap)
class Joystick:
    A = 1
    B = 2
    X = 3
    Y = 4
    LB = 5
    RB = 6
    BACK = 7
    START = 8
    LS = 9
    RS = 10

    # These are analog inputs and are different from the other buttons above
    L_STICK_X = 0
    L_STICK_Y = 1
    # They're wrapped into one, LT is positive, RT is negative, they're added together
    TRIGGERS = 2
    R_STICK_Y = 3
    R_STICK_X = 4

    # Port on the laptop that goes to the Xbox controller
    XBOX_CONTROLLER_PORT = 0

# The full map/list of ports
class RobotMap:
    pwm = PWM()
    joystick = Joystick()




# Used for testing if the RobotMap works
def test():
    print(RobotMap.pwm.drive.FR_CANTALON_MC)
    print(RobotMap.pwm.WINCH)
    print(RobotMap.joystick.A)

# Doesn't run in final code
if __name__ == "__main__":
    test()
