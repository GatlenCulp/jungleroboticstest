from magicbot import AutonomousStateMachine, timed_state, state
import wpilib

# Simply used to test if the autonomous works
class Test1:

    MODE_NAME = "Test1"
    DEFAULT = False

    def on_enable(self):
        print("Yo yo, this is the Test autonomous working")

    # tm = Time since start of auton
    def on_iteration(self, tm):
        if tm < 5:
            print(tm, "AUUUUUUUTONOOOMOUUUSS")
        elif tm < 10:
            print(tm, "REEEEEEEEEEE")
        else:
            print(tm, "TURN ME OOOOOOOOF")

    def on_disable(self):
        print("Disabled :C")
