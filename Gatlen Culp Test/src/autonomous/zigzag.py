import wpilib
from magicbot import AutonomousStateMachine, state, timed_state, default_state
from components.drive import Drive

class ZigZag(AutonomousStateMachine):

    MODE_NAME = "ZigZag"
    DEFAULT = True

    drive: Drive

    @timed_state(duration=2, next_state="swerveLeft", first=True)
    def driveStraight(self):
        speed = 0.5
        self.drive.drive_controller.tankDrive(speed, speed)

    @timed_state(duration=1.5, next_state="swerveRight")
    def swerveLeft(self):
        self.drive.drive_controller.tankDrive(0.4, 0.6)


    @timed_state(duration=1.5, next_state="swerveLeft")
    def swerveRight(self):
        self.drive.drive_controller.tankDrive(0.6, 0.4)

    @default_state
    def stopDriving(self):
        self.drive.drive_controller.tankDrive(0, 0)
