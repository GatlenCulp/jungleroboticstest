# import wpilib
#
# class Drive:
#     '''
#         This class really wasn't necessary because differential_drive is not a
#         custom component and works just fine in robot.py
#     '''
#
#
#     drive_injection_check: str
#
#     # Supposed to be a CANTalon, a Talon over CAN but I'm not sure how to set that up
#     drive_fl_motor: wpilib.Talon
#     drive_bl_motor: wpilib.Talon
#     drive_left: wpilib.SpeedControllerGroup
#
#     drive_fr_motor: wpilib.Talon
#     drive_br_motor: wpilib.Talon
#     drive_right: wpilib.SpeedControllerGroup
#
#     drive_controller: wpilib.RobotDrive
#
#     def enable(self):
#         print("==== DRIVE ENABLED ====")
#         print(self.drive_injection_check)
#
#     def is_ready(self):
#         return True
#
#     def execute(self):
#         pass
