'''
Used for calculations before compiling into the actual config.json file

Taken from the actual 2016 Fortress robot
'''

import json
from pyfrc.physics.motor_cfgs import MOTOR_CFG_CIM

robot_info = {
    # lbs
    "weight": 80,
    # inches
    "bumper_width": 3.5,
    "width": 30,
    "height": 30,

    "motors_per_side": 2,
    "motor_type": MOTOR_CFG_CIM,
    # Estimated
    "gear_ratio": 12
}

data = {
    "pyfrc": {

        "robot": {
          "w": (robot_info["width"] + (robot_info["bumper_width"] * 2))/12,
          "h": (robot_info["height"] + (robot_info["bumper_width"] * 2))/12,
          "starting_x": 5,
          "starting_y": 15
        },

        "field": {
        # Field for 2016 doesn't exist
          "season": "2017",
          "px_per_ft": 12
        }

    }
}

if __name__ == "__main__":
    with open("./config.json", "w") as f:
        json.dump(data, f, indent=4)
