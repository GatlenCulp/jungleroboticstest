'''
Created by Gatlen Culp 23 December 2018

This is the Master File that runs all the other files.
'''

import wpilib, magicbot
# from pyconsole_util import util
from robotmap import RobotMap as Map
# from components.drive import Drive

class Robot(magicbot.MagicRobot):

    def createObjects(self):
        self.drive_test_motor_1 = wpilib.Talon(0)
        self.drive_test_motor_2 = wpilib.Talon(1)
        self.drive_test_motor_3 = wpilib.Talon(2)
        self.drive_test_motor_4 = wpilib.Talon(3)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.Joystick(Map.joystick.XBOX_CONTROLLER_PORT)

    def disabledPeriodic(self):
        pass

    def teleopInit(self):
        pass

    def teleopPeriodic(self):
        a_pressed = self.xbox.getRawButton(Map.joystick.A)
        x_pressed = self.xbox.getRawButton(Map.joystick.X)
        y_pressed = self.xbox.getRawButton(Map.joystick.Y)
        b_pressed = self.xbox.getRawButton(Map.joystick.B)

        if a_pressed:
            self.drive_test_motor_1.set(1)
            print("a")
        if x_pressed:
            self.drive_test_motor_2.set(1)
            print("x")
        if y_pressed:
            self.drive_test_motor_3.set(1)
            print("y")
        if b_pressed:
            self.drive_test_motor_4.set(1)
            print("b")
        else:
            self.drive_test_motor_1.set(0)
            self.drive_test_motor_2.set(0)
            self.drive_test_motor_3.set(0)
            self.drive_test_motor_4.set(0)

if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
